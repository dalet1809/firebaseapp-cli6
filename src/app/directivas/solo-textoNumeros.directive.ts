import { Directive, ElementRef, OnInit, HostListener, Input, OnChanges } from '@angular/core';
import { PatternValidator } from '@angular/forms';

@Directive({
  selector: '[soloTextoNumeros]',
  providers: [PatternValidator],
  host: {
    '(input)': 'ngOnChanges($event)'
  }
})
export class SoloTextoNumerosDirective implements OnChanges {
  input: ElementRef;
  constructor(inputText: ElementRef) {
    inputText.nativeElement.value = inputText.nativeElement.value.toUpperCase();
    this.input = inputText;
  }

  ngOnChanges(changes): void {
      this.input.nativeElement.value = this.input.nativeElement.value.replace(/[^ÑA-Za-zñ0-9 ]/g, '');
  }

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    const evento = <KeyboardEvent>event;
    const patt = new RegExp(/[^ÑA-Za-zñ0-9 ]/g);
   //if (patt.test(evento.key.toString())) {
   //   evento.preventDefault();
   // }
    this.input.nativeElement.value = this.input.nativeElement.value.replace(/[^ÑA-Za-zñ0-9 ]/g, '');
  }
  @HostListener('keyup', ['$event']) onUpDown(event) {
    this.input.nativeElement.value = this.input.nativeElement.value.replace(/[^ÑA-Za-zñ0-9 ]/g, '');
  }

}
