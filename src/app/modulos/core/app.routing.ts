import { ModuleWithProviders, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/inicio',
        pathMatch: 'full'
    },
    {
        path: 'inicio',
        loadChildren: './../inicio/inicio.module#InicioModule'
    },
    {
        path: 'usuarios',
        loadChildren: './../usuarios/usuarios.module#UsuariosModule'
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });