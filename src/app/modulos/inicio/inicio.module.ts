import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {  MzCardModule, MzButtonModule, MzIconModule, MzInputModule, MzValidationModule, MzToastModule } from 'ngx-materialize';
import { InicioComponent } from './inicio.component';
import { AuthGuardService } from '../../servicios/auth-guard.service';
import { CompartidoModule } from '../compartido/compartido.module';

const appRoutes: Routes = [
  {
    path: '',
    component: InicioComponent,
    canActivate: [
      AuthGuardService
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
    CompartidoModule,
    MzCardModule,
    MzIconModule,
    MzButtonModule
  ],
  declarations: [
    InicioComponent
  ]
})
export class InicioModule { }
