import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from '../../../entidades/usuario';
import { GlobalService } from '../../../servicios/global.service';

import { UsuarioService } from '../../../servicios/usuario.service';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.component.html',
  styleUrls: ['./inicio-sesion.component.css'],
  providers: [
    UsuarioService
  ]
})
export class InicioSesionComponent implements OnInit {

  usuario: Usuario = new Usuario();
  usuarioRegistro: Usuario = new Usuario();

  formularioSesion: FormGroup;
  formularioRegistro: FormGroup;

  mensajesError: any = {
    clave: {
      required: 'Por favor ingrese la clave.',
      minlength: 'La clave debe ser de mínimo 6 caracteres.',
    },
    correo: {
      maxlength: 'El correo debe ser de máximo 200 caracteres.',
      pattern: 'El correo no es valido.',
      required: 'Por favor ingrese el correo electrónico.'
    },
    nombre: {
      required: 'Por favor ingrese el nombre.',
    }
  };

  constructor(
    private router: Router,
    private generador: FormBuilder,
    private servicioGlobal: GlobalService,
    private servicioUsuario: UsuarioService,
    private toastService: MzToastService,
  ) { }

  ngOnInit() {
    this.generarFormulario();
  }

  generarFormulario() {
    this.formularioSesion = this.generador.group({
      correo: [this.usuario.correo, Validators.compose([Validators.required, Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[\.][a-zA-Z]{2,4}$')])],
      clave: [this.usuario.clave, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
    this.formularioRegistro = this.generador.group({
      nombre: [this.usuarioRegistro.nombre, Validators.compose([Validators.required])],
      correo: [this.usuarioRegistro.correo, Validators.compose([Validators.required, Validators.maxLength(200), Validators.pattern('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[\.][a-zA-Z]{2,4}$')])],
      clave: [this.usuarioRegistro.clave, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  iniciarSesion() {
    if (!this.formularioSesion.invalid) {
      this.servicioGlobal.cambiarEstadoLoader(true);
      console.log(this.usuario);
      
      let operacion: Observable<any>;
      operacion = this.servicioUsuario.inicioSesion(this.usuario);
      operacion.subscribe(
        usuario => {
          console.log(usuario);
          this.router.navigate(['/inicio']);
          this.servicioGlobal.cambiarEstadoLoader(false);
        },
        error => {
          console.log(error);
          this.toastService.show(error, 4000, 'orange darken-1');
          this.servicioGlobal.cambiarEstadoLoader(false);
        }
      );

    } else {
      for (var indice in this.formularioSesion.controls) {
        this.formularioSesion.controls[indice].markAsTouched();
      }
    }
  }

  registro() {
    if (!this.formularioRegistro.invalid) {
      this.servicioGlobal.cambiarEstadoLoader(true);
      console.log(this.usuarioRegistro);

      let operacion: Observable<any>;
      operacion = this.servicioUsuario.registroUsuario(this.usuarioRegistro);
      operacion.subscribe(
        usuario => {
          console.log(usuario);
          this.router.navigate(['/inicio']);
          this.servicioGlobal.cambiarEstadoLoader(false);
        },
        error => {
          console.log(error);
          this.toastService.show(error, 4000, 'orange darken-1');
          this.servicioGlobal.cambiarEstadoLoader(false);
        }
      );


    } else {
      for (var indice in this.formularioRegistro.controls) {
        this.formularioRegistro.controls[indice].markAsTouched();
      }
    }

  }
}
