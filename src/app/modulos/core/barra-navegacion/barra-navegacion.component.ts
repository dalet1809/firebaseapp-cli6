import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { Subscription } from 'rxjs';
import { GlobalService } from '../../../servicios/global.service';

@Component({
  selector: 'app-barra-navegacion',
  templateUrl: './barra-navegacion.component.html',
  styleUrls: ['./barra-navegacion.component.css']
})

export class BarraNavegacionComponent implements OnInit, OnDestroy {

  sesionActiva: boolean = false;
  suscrpcionSesion: Subscription;

  constructor(
    private router: Router,
    private fireAuth: AngularFireAuth,
    private servicioGlobal: GlobalService
  ) { }

  ngOnInit() {
    this.suscrpcionSesion = this.servicioGlobal.sesionActiva.subscribe(
      valor => {
        this.sesionActiva = valor;
        console.log('SI paso SESION1 ' + this.sesionActiva);
      }
    );
  }

  ngOnDestroy(): void {
    this.suscrpcionSesion.unsubscribe();
  }

  cerrarSesion() {

    this.servicioGlobal.cambiarEstadoLoader(true);
    this.router.navigate(['/usuarios/inicio-sesion']);
    this.servicioGlobal.cambiarEstadoSesion(false);
    this.servicioGlobal.cambiarEstadoLoader(false);
  }

}
