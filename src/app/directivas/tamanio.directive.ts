import { Directive, ElementRef, OnInit, HostListener, Input, OnChanges } from '@angular/core';

@Directive({
    selector: '[tamanioVentana]',
    host: {
        '(window:resize)': 'onResize($event)'
    }
})

export class TamanioVentanaDirective implements OnInit, OnChanges {

    private elemento: HTMLElement;

    @Input()
    ajuste: number;

    @Input()
    atributo: string;

    constructor(private element: ElementRef) {
        this.elemento = element.nativeElement;
    }

    ngOnInit(): void {
        this.actualizarAlto();
    }

    ngOnChanges(changes): void {
        if (changes.ajuste) {
         //   console.log('PASO POR ACTUALIZAR');
            this.actualizarAlto();
        }
    }

    onResize(event: Event) {
        this.actualizarAlto();
    }

    actualizarAlto(): void {
        let alto = 'minHeight';
      //  console.log('SI PASO ACTUALIZAR ALTO');
        if (!this.ajuste) {
            this.ajuste = 0;
        }

        if(this.atributo && this.atributo != '') {
            alto = this.atributo;
        }

        let viewportwidth: number = 0;
        let viewportheight: number = 0

        if (typeof window.innerWidth != 'undefined') {
            viewportwidth = window.innerWidth;
            viewportheight = window.innerHeight;

        } else {
            if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
                viewportwidth = document.documentElement.clientWidth;
                viewportheight = document.documentElement.clientHeight;
            } else {
                viewportwidth = document.getElementsByTagName('body')[0].clientWidth;
                viewportheight = document.getElementsByTagName('body')[0].clientHeight;
            }
        }

        this.elemento.style[alto] = viewportheight - this.ajuste + 'px';
    }

}