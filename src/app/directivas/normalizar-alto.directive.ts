import { Directive, ElementRef, Input, OnInit, HostListener } from '@angular/core';

@Directive({
    selector: '[normalizarAlto]',
    host: {
        '(window:resize)': 'onResize($event)'
    }
})

export class NormalizarAltoDirective implements OnInit {

    @Input()
    normalizarAlto: any;

    constructor(private elemento: ElementRef) {

    }

    ngOnInit(): void {
        this.ngNormalizarAlto(this.elemento.nativeElement, this.normalizarAlto);
    }

    onResize(event: Event) {
        this.ngNormalizarAlto(this.elemento.nativeElement, this.normalizarAlto);
    }

    ngNormalizarAlto(elementoPadre: HTMLElement, clase: string): void {
        console.log(clase);
        if (elementoPadre) {
            let elementosHijo = elementoPadre.getElementsByClassName(clase);

            if (elementosHijo) {
                Array.from(elementosHijo).forEach((hijo: HTMLElement) => {
                    hijo.style.height = 'initial';
                });

                let alturas = Array.from(elementosHijo).map(hijo => hijo.getBoundingClientRect().height);

                let alturaMaxima = alturas.reduce((anterior, actual) => {
                    return actual > anterior ? actual : anterior;
                });

                Array.from(elementosHijo).forEach((hijo: HTMLElement) => hijo.style.height = alturaMaxima + 'px');

            } else {
                return;
            }
        } else {
            return;
        }
    }
}