import { Directive, ElementRef, OnInit, HostListener, Input, OnChanges } from '@angular/core';
import { PatternValidator } from '@angular/forms';

@Directive({
  selector: '[soloTextoNumerosCaracteres]',
  providers: [PatternValidator],
  host: {
    '(input)': 'ngOnChanges($event)'
  }
})
export class SoloTextoNumerosCaracteresDirective implements OnChanges {
  input: ElementRef;
  constructor(inputText: ElementRef) {
    inputText.nativeElement.value = inputText.nativeElement.value.toUpperCase();
    this.input = inputText;
  }

  ngOnChanges(changes): void {
      this.input.nativeElement.value = this.input.nativeElement.value.replace(/[^ÑÁÉÍÓÚñáéíóúA-Za-zñ0-9 \u002E\u002D\u0040\u005F\u0026\u002C]/g, '');
  }

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    const evento = <KeyboardEvent>event;
    const patt = new RegExp(/[^ÑÁÉÍÓÚñáéíóúA-Za-zñ0-9 \u002E\u002D\u0040\u005F\u0026\u002C]/g);
    if (patt.test(evento.key.toString())) {
      evento.preventDefault();
    }
    this.input.nativeElement.value = this.input.nativeElement.value.replace(/[^ÑÁÉÍÓÚñáéíóúA-Za-zñ0-9 \u002E\u002D\u0040\u005F\u0026\u002C]/g, '');
  }
  @HostListener('keyup', ['$event']) onUpDown(event) {
    this.input.nativeElement.value = this.input.nativeElement.value.replace(/[^ÑÁÉÍÓÚñáéíóúA-Za-zñ0-9 \u002E\u002D\u0040\u005F\u0026\u002C]/g, '');
  }

}
