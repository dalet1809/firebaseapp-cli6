import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from './modulos/core/core.module';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { PrincipalComponent } from './modulos/core/principal/principal.component';

import { GlobalService } from './servicios/global.service';
import { AuthGuardService } from './servicios/auth-guard.service';

import { environment } from '../environments/environment';
@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule,
    CoreModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase, 'TallerServerless'),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [
    GlobalService,
    AuthGuardService
  ],
  bootstrap: [PrincipalComponent]
})
export class AppModule { }
