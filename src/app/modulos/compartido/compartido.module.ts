import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TamanioVentanaDirective } from "../../directivas/tamanio.directive";
import { SlimScroll } from '../../directivas/slimscroll.directive';
import { NormalizarAltoDirective } from '../../directivas/normalizar-alto.directive';
import { SoloTextoNumerosDirective } from '../../directivas/solo-textoNumeros.directive';
import { SoloTextoNumerosCaracteresDirective } from '../../directivas/solo-textoNumerosCaracteres.directive';
import { TextoMayusculasDirective } from '../../directivas/texto-mayusculas.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    SlimScroll,
    TamanioVentanaDirective,
    NormalizarAltoDirective,
    SoloTextoNumerosDirective,
    SoloTextoNumerosCaracteresDirective,
    TextoMayusculasDirective
  ],
  exports: [
    SlimScroll,
    TamanioVentanaDirective,
    NormalizarAltoDirective
  ]
})
export class CompartidoModule { }
