import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Usuario } from '../../entidades/usuario';
import { GlobalService } from '../../servicios/global.service';
import { UsuarioService } from '../../servicios/usuario.service';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
  providers: [
    UsuarioService
  ]
})
export class UsuariosComponent implements OnInit {
  usuarios: Usuario[] = [];
  suscriptor: Subscription;

  constructor(
    private servicioGlobal: GlobalService,
    private servicioUsuario: UsuarioService,
    private toastService: MzToastService,
  ) { }

  ngOnInit() {
    this.servicioGlobal.cambiarEstadoLoader(true);

    let operacion: Observable<any>;
    operacion = this.servicioUsuario.cargarUsuarios();
    operacion.subscribe(
      usuarios => {
        console.log(usuarios);
        this.usuarios = usuarios;
      },
      error => {
        console.log(error);
        this.toastService.show(error, 4000, 'orange darken-1');
        this.servicioGlobal.cambiarEstadoLoader(false);
      }
    );

    this.servicioGlobal.cambiarEstadoLoader(false);
  }

  ngOnDestroy() {
    try {
      this.suscriptor.unsubscribe();
    } catch (error) { }
  }

}
