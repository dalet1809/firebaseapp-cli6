import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable, Subscription, throwError } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { GlobalService } from './global.service';
import { Usuario } from '../entidades/usuario';


@Injectable()
export class UsuarioService {

    constructor(
        private servicioGlobal: GlobalService
    ) {

    }

    cargarUsuarios(): Observable<Usuario[]> {
        let suscriptor: Subscription;
        let usuarios: Usuario[] = [];

        return new Observable<Usuario[]>((observador) => {

            observador.next(usuarios);

        });
    }

    inicioSesion(usuario: Usuario): Observable<any> {
        return new Observable<any>((observador) => {

            observador.next('xxxxxx');
            this.servicioGlobal.cambiarEstadoSesion(true);
            observador.complete();


        });
    }

    registroUsuario(usuario: Usuario): Observable<any> {

        return new Observable<any>((observador) => {

            usuario.clave = '';
            usuario.fecha = new Date().getTime();
            this.servicioGlobal.cambiarEstadoSesion(true);
            observador.next(usuario);
            observador.complete();

        });
    }
}