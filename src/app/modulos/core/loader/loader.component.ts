import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { GlobalService } from '../../../servicios/global.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit, OnDestroy {

  loaderActivo: boolean = true;
  suscrpcionLoader: Subscription;

  constructor(
    private servicioGlobal: GlobalService
  ) { }

  ngOnInit(): void {
    this.suscrpcionLoader = this.servicioGlobal.loaderActivo.subscribe(
      valor => {
        //	console.log("El valor se actualizo, loaderActivo: " + valor);
        this.loaderActivo = valor;
      }
    );
  }

  ngOnDestroy(): void {
    this.suscrpcionLoader.unsubscribe();
  }

}
