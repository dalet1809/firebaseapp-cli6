export class Usuario {

    indice: string;
    nombre: string;
    correo: string;
    clave: string;
    fecha: number | Date;


    constructor() {
        this.indice = '';
        this.nombre = '';
        this.correo = '';
        this.clave = '';
        this.fecha = 0;
    }
}