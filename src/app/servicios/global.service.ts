import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class GlobalService {
    private loaderActivoFuente = new BehaviorSubject<boolean>(false);
    private loaderContadorFuente = new BehaviorSubject<number>(0);
    private sesionActivaFuente = new BehaviorSubject<boolean>(false);
    public loaderActivo = this.loaderActivoFuente.asObservable();
    public sesionActiva = this.sesionActivaFuente.asObservable();
    private vistasResumen: number = 0;

    private barraLateralFuente = new BehaviorSubject<boolean>(false);
    public barraLateral = this.barraLateralFuente.asObservable();

    cambiarEstadoLoader(valor: boolean): void {
        //console.log('CAMBIAR ESTADO LOADER ' + valor);
        this.vistasResumen = this.loaderContadorFuente.getValue();
        // console.log('CONTADOR LOADER', this.vistasResumen);

        if (valor) {
            this.vistasResumen++;
        } else {
            if (this.vistasResumen > 0) {
                this.vistasResumen--;
            }
        }

        this.loaderContadorFuente.next(this.vistasResumen);
        // console.log('CONTADOR LOADER', this.vistasResumen);
        if (!valor) {
            if (this.vistasResumen == 0) {
                this.loaderActivoFuente.next(valor);
            }
        } else {
            this.loaderActivoFuente.next(valor);
        }

    }

    getValorEstadoLoader(): boolean {
        return this.loaderActivoFuente.getValue();
    }

    mostrarBarraLateral(valor: boolean): void {
        this.barraLateralFuente.next(valor);
    }

    getValorBarraLateral(): boolean {
        return this.barraLateralFuente.getValue();
    }

    cambiarEstadoSesion(valor: boolean) {
        console.log('CAMBIO VALOR ' + valor);
        this.sesionActivaFuente.next(valor);
    }

    getValorEstadoSesion(): boolean {
        return this.sesionActivaFuente.getValue();
    }
}