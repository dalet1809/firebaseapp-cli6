import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './app.routing';
import { MzNavbarModule, MzIconModule } from 'ngx-materialize'
import { BarraNavegacionComponent } from './barra-navegacion/barra-navegacion.component';
import { LoaderComponent } from './loader/loader.component';
import { PiePaginaComponent } from './pie-pagina/pie-pagina.component';
import { PrincipalComponent } from './principal/principal.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    MzNavbarModule,
    MzIconModule
  ],
  declarations: [BarraNavegacionComponent, LoaderComponent, PiePaginaComponent, PrincipalComponent],
  exports: [PrincipalComponent]
})
export class CoreModule { }
