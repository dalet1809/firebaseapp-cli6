import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MzTabModule, MzCardModule, MzButtonModule, MzIconModule, MzInputModule, MzValidationModule, MzToastModule } from 'ngx-materialize';
import { InicioSesionComponent } from './inicio-sesion/inicio-sesion.component';
import { UsuariosComponent } from './usuarios.component';
import { AuthGuardService } from '../../servicios/auth-guard.service';
import { CompartidoModule } from '../compartido/compartido.module';

const appRoutes: Routes = [
  {
    path: '',
    component: UsuariosComponent,
    canActivate: [
      AuthGuardService
    ]
  },
  {
    path: 'inicio-sesion',
    component: InicioSesionComponent,
    canActivate: [
      AuthGuardService
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(appRoutes),
    MzTabModule,
    MzCardModule,
    MzButtonModule,
    MzIconModule,
    MzInputModule,
    MzValidationModule,
    MzToastModule,
    CompartidoModule
  ],
  declarations: [InicioSesionComponent, UsuariosComponent]
})
export class UsuariosModule { }
