import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../servicios/global.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  constructor(
    private servicioGlobal: GlobalService
  ) { }

  ngOnInit() {
    console.log('Si paso principal');
  }

}
