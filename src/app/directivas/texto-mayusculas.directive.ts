import { Directive, ElementRef, OnInit, HostListener, Input, OnChanges } from '@angular/core';

@Directive({
    selector: 'input[type=text]',
    host: {
        '(input)': 'element.nativeElement.value=$event.target.value.toUpperCase()'
    }
})

export class TextoMayusculasDirective {
    private elemento: HTMLElement;

    constructor(private element: ElementRef) {
    }
}