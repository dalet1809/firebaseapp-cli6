import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { GlobalService } from './global.service';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private router: Router,
        private servicioGlobal: GlobalService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        console.log(route);
        console.log(route.parent);
        let modulo = '';
        if (route.url.length == 0) {
            modulo = route.parent.url[0].path;
        } else {
            modulo = route.url[0].path;
        }

        console.log(modulo);
        
        if (modulo == 'inicio-sesion') {
            console.log('SI PASO ');
            this.servicioGlobal.cambiarEstadoLoader(true);
            return new Observable<boolean>((observador) => {
                setTimeout(() => {

                    if (!this.servicioGlobal.getValorEstadoSesion()) {
                        observador.next(true);
                    } else {
                        let link = ['/inicio'];
                        this.router.navigate(link);
                        observador.next(false);
                    }


                    observador.complete();
                    this.servicioGlobal.cambiarEstadoLoader(false);
                }, 1100);

            });
        } else {
            this.servicioGlobal.cambiarEstadoLoader(true);
            return new Observable<boolean>((observador) => {

                setTimeout(() => {

                    if (!this.servicioGlobal.getValorEstadoSesion()) {
                        console.log('NO EXISTE SESION');
                        let link = ['/usuarios/inicio-sesion'];
                        this.router.navigate(link);
                        this.servicioGlobal.cambiarEstadoSesion(false);
                        observador.next(false);
                    } else {
                        console.log('EXISTE SESION');
                        this.servicioGlobal.cambiarEstadoSesion(true);
                        observador.next(true);
                    }

                    observador.complete();
                    this.servicioGlobal.cambiarEstadoLoader(false);
                }, 1100);

                console.log('Si paso GUARDIA');
            });
        }
    }
}