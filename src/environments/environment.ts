// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyDDboc4bR8bC0a6HxXBiHIf9rugxFS9kGc",
    authDomain: "fir-app-bf7a8.firebaseapp.com",
    databaseURL: "https://fir-app-bf7a8.firebaseio.com",
    projectId: "fir-app-bf7a8",
    storageBucket: "fir-app-bf7a8.appspot.com",
    messagingSenderId: "40730365423"
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
